﻿namespace BusinessObjects
{
    // ** Enterprise Design Pattern: Domain Model, Identity Field, Foreign key mapping


    public class OrderDetail : BusinessObject
    {
        public int OrderId { get; set; }

        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public int Quantity { get; set; }
        public double UnitPrice { get; set; }
        public double Discount { get; set; }

        public Order Order { get; set; }
    }
}