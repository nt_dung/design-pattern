﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using BusinessObjects;

namespace DataObjects.EntityFramework
{
    public class CategoryDao : ICategoryDao
    {
        static CategoryDao()
        {
            Mapper.CreateMap<CategoryEntity, Category>();
        }

        public List<Category> GetCategories()
        {
            using (var context = new actionEntities())
            {
                var categories = context.CategoryEntities.ToList();
                return Mapper.Map<List<CategoryEntity>, List<Category>>(categories);
            }
        }

        public Category GetCategoryByProduct(int productId)
        {
            using (var context = new actionEntities())
            {
                var product = context.ProductEntities.SingleOrDefault(p => p.ProductId == productId);
                var category = context.CategoryEntities.SingleOrDefault(c => c.CategoryId == product.CategoryId);

                return Mapper.Map<CategoryEntity, Category>(category);
            }
        }
    }
}