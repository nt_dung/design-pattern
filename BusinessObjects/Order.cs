﻿using System;
using System.Collections.Generic;

namespace BusinessObjects
{
    // ** Enterprise Design Pattern: Domain Model, Identity Field, Foreign Key Mapping.

    public class Order : BusinessObject
    {
        public int OrderId { get; set; }

        public DateTime OrderDate { get; set; }
        public DateTime RequiredDate { get; set; }
        public double Freight { get; set; }

        public Member Member { get; set; }

        public List<OrderDetail> OrderDetails { get; set; }
    }
}