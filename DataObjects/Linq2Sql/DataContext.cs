﻿using System.Configuration;

namespace DataObjects.Linq2Sql
{
    // ActionDataContext with the appriate connectionstring

    public class DataContext : ActionDataContext
    {
        private static readonly string connectionString = ConfigurationManager.ConnectionStrings["Action"].ConnectionString;

        // constructor
        public DataContext() : base(connectionString)
        {
        }
    }
}