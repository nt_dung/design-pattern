﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;

namespace DataObjects.Linq2Sql
{
    public class OrderDetailDao : IOrderDetailDao
    {
        static OrderDetailDao()
        {
            Mapper.CreateMap<OrderDetail, BusinessObjects.OrderDetail>();
        }

        public List<BusinessObjects.OrderDetail> GetOrderDetails(int orderId)
        {
            using (var context = DataContextFactory.CreateContext())
            {
                var orderDetails = context.OrderDetails.Where(d => d.OrderId == orderId).ToList();
                return Mapper.Map<List<OrderDetail>, List<BusinessObjects.OrderDetail>>(orderDetails);
            }
        }
    }
}