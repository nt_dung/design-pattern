﻿namespace BusinessObjects
{
    // ** Enterprise Design Pattern: Domain Model, Identity Field, Foreign key mapping

    public class Product : BusinessObject
    {
        public int ProductId { get; set; }

        public string ProductName { get; set; }
        public string Weight { get; set; }
        public double UnitPrice { get; set; }
        public int UnitsInStock { get; set; }

        public Category Category { get; set; }
    }
}