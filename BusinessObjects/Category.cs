﻿namespace BusinessObjects
{
    // ** Enterprise Design Pattern: Domain Model, Identity Field

    public class Category : BusinessObject
    {
        public int CategoryId { get; set; }

        public string CategoryName { get; set; }
        public string Description { get; set; }
    }
}