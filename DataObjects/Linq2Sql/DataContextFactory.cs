﻿using System.Configuration;
using System.Data.Linq.Mapping;

namespace DataObjects.Linq2Sql
{
    // ** Factory pattern

    public static class DataContextFactory
    {
        private static readonly string connectionString;
        private static readonly MappingSource mappingSource;

        static DataContextFactory()
        {
            connectionString = ConfigurationManager.ConnectionStrings["Action"].ConnectionString;

            var context = new ActionDataContext(connectionString);
            mappingSource = context.Mapping.MappingSource;
        }

        // ** Factory method. creates a new DataContext using cached connectionstring

        public static ActionDataContext CreateContext()
        {
            return new ActionDataContext(connectionString, mappingSource);
        }
    }
}