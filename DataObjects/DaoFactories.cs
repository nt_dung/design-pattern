﻿using DataObjects.AdoNet;

namespace DataObjects
{
    // Factory of factories. This class is a factory class that creates
    // data-base specific factories which in turn create data acces objects.
    // ** GoF Design Patterns: Factory.

    public class DaoFactories
    {
        // ** GoF Design Pattern: Factory

        public static IDaoFactory GetFactory(string dataProvider)
        {
            switch (dataProvider.ToLower())
            {
                case "ado.net":
                    return new DaoFactory();
                case "linq2sql":
                    return new Linq2Sql.DaoFactory();
                case "entityframework":
                    return new EntityFramework.DaoFactory();

                default:
                    return new DaoFactory();
            }
        }
    }
}